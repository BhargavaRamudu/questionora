const express = require('express');
const app = express();

const productroutes = require('./routes/products');
const orderroutes = require('./routes/orders');
const userroutes = require('./routes/users');
const commentroutes = require('./routes/comments');
const questionroutes = require('./routes/questions');
const answerroutes = require('./routes/answers');

// const morgan = require('morgan');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');

const logger = require('./logger');

// app.use(morgan('combined'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

mongoose.Promise = global.Promise;


let error = 'this is error';

logger.info('Logging is working');

app.use((req, res, next) => {

	// logger.info({requestData: req.body}, 'this is for msg');
	
	// childLogger = logger.child({additionalField: 'This is a child logger'});
	// childLogger.info('got the request');
	
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Accept, Content-Type, Authorization')
	if (req.method === 'OPTIONS') {
		res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
		return res.status(200).json({});
	};
	next();
})



mongoose.connect(<Please enter your mongodb connection url here>,
				{ useNewUrlParser: true }
				)
				.then(result => {
					console.log('success');

				})
				.catch(err => {
					console.log(err);
				});

app.use('/users', userroutes);
app.use('/questions', questionroutes);
app.use('/answers', answerroutes);
// Routes which should handle the requests
app.use('/products', productroutes);
app.use('/orders', orderroutes);

app.use((req, res, next) => {
	const error = new Error('Not found');
	error.status = 404;
	next(error);
}) 


app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
		error: {
			message: error.message
		}
	});
});

module.exports = app;

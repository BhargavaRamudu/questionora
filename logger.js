const bunyan = require('bunyan');


const log = bunyan.createLogger({
    name: 'sloss-logger',
    level: 'info',
    streams: [
        {
            level: 'warn',
            stream: process.stdout
        },
        {
            level: 'info',
            path: './info.log'
        }
    ],
    
    customVariable: 'damn'
});



module.exports = log;
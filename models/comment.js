const mongoose = require('mongoose');


const commentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    content: { type: String, required: true, maxlength: 250 },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now, select: false },
    question: { type: mongoose.Schema.Types.ObjectId, ref: 'Question', select: false },
    answer: { type: mongoose.Schema.Types.ObjectId, ref: 'Answer', select: false },
    parentComment: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment', default: null, select: false }]
})


module.exports = mongoose.model('Comment', commentSchema);


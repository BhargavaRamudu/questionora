const mongoose = require('mongoose');


const answerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    content: { type: String, required: true },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now},
    questionId: { type: mongoose.Schema.Types.ObjectId, ref: 'Question' }
})


module.exports = mongoose.model('Answer', answerSchema);


const mongoose = require('mongoose');

const questionSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: { type: String, text: true},
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now } 
});


module.exports = mongoose.model('Question', questionSchema);


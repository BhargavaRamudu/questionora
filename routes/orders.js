const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/order');

router.get('/', (req, res, next) => {
	res.status(200).json({
		message: 'orders were fetched'
	});
});

router.post('/', (req, res, next) => {
	const order = new Order({
		_id: new mongoose.Types.ObjectId(),
		product: req.body.productId,
		quanity: req.body.quantity
	});

	order
		.save()
		.then(result => {
			console.log(result);
			res.status(201).json(result);
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			})
		})
})



router.get('/:orderId', (req, res, next) => {
	res.status(200).json({
		message: 'particular order was fetched'
	})
})



router.delete('/:orderId', (req, res, next) => {
	res.status(200).json({
		message: 'particular order was deleted'
	});
});




module.exports = router;
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multiparty = require('multiparty');

const { checklogin, parseForm } = require('../utils/check-login');
const Product = require('../models/product');

router.get('/', (req, res, next) => {
	Product.find()
		.select('name price _id')
		.exec()
		.then(result => {
			const response = {
				count: result.length,
				products: result.map(result => {
					
					return {
						name: result.name,
						price: result.price,
						_id: result._id,
						request: {
							type: 'GET',
							url: `http://localhost:3000/products/${result._id}`
						}
					}
				})
			}
			res.status(200).json(response);
			
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				message: "there is some error with this query route and check it once please"
			})
		})
	
});

router.post('/',checklogin, parseForm, function(req, res, next) {
	// console.log(fields)
	// const product = new Product({
	// 	_id: new mongoose.Types.ObjectId(),
	// 	name: res.locals.formFields.name[0],
	// 	price: res.locals.formFields.price[0]
	// });	
	// const product = new Product({
	// 	_id: new mongoose.Types.ObjectId(),
	// 	name: fields.name[0],
	// 	price: fields.price[0]
	// });

	const product = new Product({
		_id: new mongoose.Types.ObjectId(),
		name: req.formData.name[0],
		price: req.formData.price[0]
	});
	product
		.save()
		.then(result => {
			// console.log(result);
			res.status(201).json({
				message: 'handling products post request',
				createdProduct: product
			});
			
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: "fuck"
			})
			
		})
	
});

router.get('/:productId', function(req, res, next) {
	const id = req.params.productId;
	Product.findById(id)
		.exec()
		.then(doc => {
			console.log(doc);
			if(doc) {
				res.status(200).json(doc);
			} else {
				res.status(404).json({
					message: "no valid entry for provided Id"
				})
			}
			
		})
		.catch(err => {
			console.log(err)
			res.status(500).json({error: err});

		});
})


router.patch('/:productId',checklogin, (req, res, next) => {

	const updateOps = {};
	for (const ops of req.body) {
		updateOps[ops.propName] = ops.value;
	}
	Product.update({_id: req.params.productId}, { $set: updateOps })	
	.exec()
	.then(result => {
		console.log(result);
		res.status(200).json({
			message: 'updated',
			product: {
				request: 'GET',
				url: `localhost:3000/products/${req.params.productId}`
			}

		});
	})
	.catch(err => {
		res.status(500).json(err);
		

	})
	
})




router.delete('/:productId',checklogin, (req, res, next) => {
	Product.remove({_id : req.params.productId})
		.exec()
		.then(result => {
			res.status(200).json(result);
		})
		.catch(err => {
			console.log(err);
			res.status(500).json(err);
		})
})



router.delete('/', checklogin, (req, res, next) => {
	Product.find({})
		.exec()
		.then(products => {
			const totalproducts = products.length;
			let i = 0;
			products.map(product => {
				Product.remove({_id: product._id})
					.exec()
					.then(result => {
						i++;
					})
					.catch(err => {
						console.log(err);
					})
			})

		})
		.catch(err => {
			console.log(err);
		})
})

module.exports = router;
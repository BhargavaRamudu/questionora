const mongoose = require('mongoose');
const Question = require('../models/question');
const router = require('express').Router();

const Answer = require('../models/answer');

router.get('/searchtags', (req, res, next) => {
    
    Question.find()
        .where('title').in([...searchtags])
        .exec()
        .then(questions => {
            res.status(200).json({
                questionlist: questions
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
})


router.get('/:questionId', (req, res, next) => {
    Answer.find({questionId: req.params.questionId})
        .sort({upadated_at: -1})
        .exec()
        .then(answers => {
            res.status(200).json({
                answers_list: answers
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
})



router.post('/', (req, res, next) => {
    const question = new Question({
        _id: new mongoose.Types.ObjectId,
        title: req.params.title,
        upadated_at: Date.now(),
        created_at: Date.now()

    })

    question.save()
        .then(result => {
            res.status(200).json({
                msg: 'successfully created the question',
                result: result
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
})


router.delete('/:questionId', (req, res, next) => {
    Question.remove({_id: req.params.questionId})
        .exec()
        .then(result => {
            res.status(200).json({
                msg: 'successfully deleted the question'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
})


module.exports = router;
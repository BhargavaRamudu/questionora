const Comment = require('../models/comment');
const mongoose = require('mongoose');
const router = require('express').Router();



router.get('/', (req, res, next) => {
    console.log('getting the comments');
    if(req.body.parentcomment) {
        commentQuery = Comment.find({answer: req.body.a_id, parentComment: req.body.parentComment})
        console.log('got the comment query with parent comment id');
    } else {
        commentQuery = Comment.find({answer: req.body.a_id});
        console.log('got the comment query object without parent comment id');
    }
    commentQuery
        .sort({updated_at: -1})
        .then(result => {
            res.status(200).json({
                commentData: result
            })
            console.log('got all comments');
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
            console.log('error fetching all comments');
        })
})
    

router.post('/', (req, res, next) => {
   
    const comment = new Comment({
        _id: new mongoose.Types.ObjectId,
        content: req.body.commentContent,
        created_at: Date.now(),
        upadated_at: Date.now(),
        questionId: req.body.q_id,
        answer: req.body.a_id,
        parentComment: req.body.parentId !== null ? req.body.parentId : null 

    })

    console.log('creating comment');

    comment.save()
        .then(result => {
            res.status(200).json({
                msg: 'successfully created the comment'
            });
            console.log('created the comment');
        })
        .catch(err => {
            res.status(500).json({
                msg: 'not created, there is something wrong'
            })

            console.log('error creating comment');
        })
})




router.delete('/:commentId', (req, res, next) => {
    console.log('deleting the comment')
    Comment.remove({_id: commentId})
        .exec()
        .then(result => {
            res.status(200).json({
                msg: 'comment successfully deleted'
            })
            console.log('deleted the comment');
        })
        .catch(err => {
            res.status(500).json({
              error: err
            })
            console.log('error deleting the comment');
        })
})



module.exports = router;
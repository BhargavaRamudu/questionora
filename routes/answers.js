const Answer = require('../models/answer');
const mongoose = require('mongoose');
const router = require('express').Router();




router.post('/', (req, res, next) => {
    const answer = new Answer({
        _id: new mongoose.Types.ObjectId,
        created_at: Date.now(),
        upadated_at: Date.now(),
        content: req.body.answer,
        questionId: req.body.q_id

    })

    answer.save()
        .then(result => {
            res.status(200).json({
                msg: 'successfully created the answer'
            })
        })
        .catch(err => {
            res.status(500).json({
                msg: 'not created, there is something wrong'
            })
        })
})


router.get('/:answerId', (req, res, next) => {
    Answer.find({_id: req.params.answerId})
        .exec()
        .then(answer => {
            res.status(200).json({
               answer: answer
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
})


router.delete('/:answerId', (req, res, next) => {
    Answer.remove({_id: answerId})
        .exec()
        .then(result => {
            res.status(200).json({
                msg: 'answer successfully deleted'
            })
        })
        .catch(err => {
            res.status(500).json({
              error: err
            })
        })
})


module.exports = router;